﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartyParty.Model;
using SmartyParty.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Labs.Controls;

namespace SmartyParty.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class OverviewPage : ContentPage
	{
	    
        public OverviewPage()
		{
			InitializeComponent ();
		    BindingContext = new OverviewViewModel();
		}

      

        private async Task Button_ClickedAsync(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new TodayPage());
        }
    }
}