﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartyParty.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmartyParty.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TodayPage : ContentPage
    { 
        public TodayPage()
        {
            InitializeComponent();
            BindingContext = new TodayViewModel();
            
        }
        public void OnChangeChecked(object sender, EventArgs args)
        {
            ((TodayViewModel) BindingContext).OnChangeChecked(sender,args);
        }
    }

}