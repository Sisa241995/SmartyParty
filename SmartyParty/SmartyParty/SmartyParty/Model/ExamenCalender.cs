﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using Android.Util;
using Newtonsoft.Json;

namespace SmartyParty.Model
{
    class ExamenCalender : BaseModel
    {

        private const int DAYS_BETWEEN_BREAKS = 3;

        private static ExamenCalender _instance;
        public ObservableCollection<Examen> ExamensOfSemester { get; }
        public ObservableCollection<Day> DaysOfLerningtime { get; }


        public Day Today
        {
            get { return DaysOfLerningtime.First(day => day.Date.Day == DateTime.Now.Day && day.Date.Month == DateTime.Now.Month &&
                    day.Date.Year == DateTime.Now.Year); }
        }

        public ExamenCalender()
        {
            ExamensOfSemester = new ObservableCollection<Examen>();
            ExamensOfSemester.Add(new Examen(new DateTime(2017,12, 16), "bildgebende Verfahren", 5 ));
            ExamensOfSemester.Add(new Examen(new DateTime(2017,12,25), "andere Klausur", 4 ));
            DaysOfLerningtime = new ObservableCollection<Day>();
            _recalculate();
        }

        public void AddExamen(Examen examen)
        {
            ExamensOfSemester.Add(examen);
            _recalculate();
        }

        public void removeExamen(Examen examen)
        {
            if (ExamensOfSemester.Remove(examen))
            {
                _recalculate();
            }
        }


        public void _recalculate()
        {
            var lastExam = (from e in ExamensOfSemester orderby e.Date descending select e).First();
            var remainingDaysPerExam = ExamensOfSemester.ToDictionary(ex => ex, ex => ex.DaysToLearn - ex.DaysLearned);

            var learnList = new List<DayHolder>();
            var currentDate = lastExam.Date;

            // Create minimal schedule
            while ((from e in remainingDaysPerExam.Values where e > 0 select e).ToArray().Length != 0)
            {
                var availableExams =
                    (from e in ExamensOfSemester where e.Date >= currentDate && remainingDaysPerExam[e] > 0 select e)
                        .ToList();
                if (availableExams.Count > 0)
                {
                    var ex = availableExams.First();
                    learnList.Insert(0, new DayHolder(ex));
                    remainingDaysPerExam[ex]--;
                }
                else
                {
                    learnList.Insert(0, new DayHolder());
                }

                currentDate = currentDate.Subtract(new TimeSpan(1, 0, 0, 0));
            }

            // Insert pauses in interval
            var learnListWPauses = new List<DayHolder>();
            var placed = 0;
            for (int i = 0; i < learnList.Count; i++)
            {
                if (learnList[i].IsFree)
                {
                    placed = 0;
                    learnListWPauses.Add(learnList[i]);
                    continue;
                }

                learnListWPauses.Add(learnList[i]);
                placed++;

                if (placed == DAYS_BETWEEN_BREAKS && i != learnList.Count - 1)
                {
                    // remove a pause from a previous double pause if possible
                    for (int loi = learnListWPauses.Count - 1; loi >= 0; loi--)
                    {
                        if (learnListWPauses[loi].IsFree && learnListWPauses[loi + 1].IsFree)
                        {
                            learnListWPauses.RemoveAt(loi);
                            break;
                        }
                    }

                    if (!learnList[i + 1].IsFree)
                    {
                        learnListWPauses.Add(new DayHolder());
                    }

                    placed = 0;
                }
            }

            List<Day> finalDays = new List<Day>();

            currentDate = lastExam.Date;
            for (int i = learnListWPauses.Count - 1; i >= 0; i--)
            {
                var todaysExam = (from e in ExamensOfSemester where e.Date == currentDate select e).ToList();

                if (todaysExam.Count > 0)
                {
                    var examDateToIns = new Day(currentDate);
                    examDateToIns.ExamendToLearn = todaysExam[0];
                    examDateToIns.isExaminationDay = true;
                    finalDays.Insert(0, examDateToIns);
                    i++;
                }
                else
                {
                    var currentHolder = learnListWPauses[i];
                    if (currentHolder.IsFree)
                    {
                        finalDays.Insert(0, new Day(currentDate));
                    }
                    else
                    {
                        var examDateToIns = new Day(currentDate);
                        examDateToIns.ExamendToLearn = currentHolder.toLearnFor;
                        finalDays.Insert(0, examDateToIns);
                    }
                }

                currentDate = currentDate.Subtract(new TimeSpan(1, 0, 0, 0));
            }
            DaysOfLerningtime.Clear();
            finalDays.ForEach(day => DaysOfLerningtime.Add(day));
        }


        public static void Save()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string filePath = Path.Combine(path, "examenCalender.txt");
            using (var file = File.Open(filePath, FileMode.CreateNew, FileAccess.Write))
            using (var strm = new StreamWriter(file))
            {
                strm.Write(JsonConvert.SerializeObject(GetInstance()));
            }
        }

        public static ExamenCalender GetInstance()
        {
            if (_instance == null)
            {
                _instance = _loadFromFile();
                if (_instance == null)
                {
                   _instance = new ExamenCalender();
                }
            }
            return _instance;
        }

        private static ExamenCalender _loadFromFile()
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                string filePath = Path.Combine(path, "examenCalender.txt");
                using (var file = File.Open(filePath, FileMode.Open, FileAccess.Read))
                using (var strm = new StreamReader(file))
                {
                    return JsonConvert.DeserializeObject<ExamenCalender>(strm.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        private class DayHolder
        {
            public Examen toLearnFor;
            public bool IsFree;

            public DayHolder(Examen toLearnFor)
            {
                this.toLearnFor = toLearnFor;
                this.IsFree = false;
            }

            public DayHolder()
            {
                this.toLearnFor = null;
                this.IsFree = true;
            }

            public override string ToString()
            {
                return this.IsFree ? "Free" : toLearnFor.Name;
            }
        }
    }
}