﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartyParty.Model
{
    class Examen : BaseModel
    {
        public DateTime Date { get; }
        public string Name { get; }
        public int DaysToLearn { get; }
        private int _daysLearned;
        
        public string DateString => Date.Day + "." + Date.Month + ".";

        public string Progress => DaysLearned + "/" + DaysToLearn;

        public int DaysLearned
        {
            get => _daysLearned;
            set
            {
                if (value >= 0) _daysLearned = value;
            }
        }

        public Examen(DateTime date, string name, int daysToLearn)
        {
            Date = date;
            Name = name;
            DaysToLearn = daysToLearn;
            DaysLearned = 0;
        }
    }
}
