﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartyParty.Model
{
    class Day : BaseModel
    {

        public bool isExaminationDay { get; set; }
        

        public string Name { get { return ExamendToLearn.Name; } }

        public string DateString { get { return Date.Day + "." + Date.Month + "." + Date.Year; } }

        public DateTime Date { get; }
        public Examen ExamendToLearn { get; set; }
        private Boolean _isChecked;

        public Boolean isChecked
        {
            get => _isChecked;
            set
            {
                decreaseOrIncreaseLearnedDays(value);
                _isChecked = value;
            }
        }

        private void decreaseOrIncreaseLearnedDays(bool value)
        {
            if (ExamendToLearn != null)
            {
                if (value)
                {
                    ExamendToLearn.DaysLearned = ExamendToLearn.DaysLearned + 1;
                }
                else
                {
                    ExamendToLearn.DaysLearned = ExamendToLearn.DaysLearned - 1;
                }
            }
        }
       

     

        public Day(DateTime date)
        {

            Date = date;
            _isChecked = false;
            ExamendToLearn = null;
            isExaminationDay = false;
        }

        public override string ToString()
        {
            var resultString = "Date: " + this.Date + ": ";

            if (isExaminationDay)
            {
                return resultString + "EXAM TODAY: " + ExamendToLearn.Name;
            }

            if (this.ExamendToLearn == null)
            {
                return resultString + "Free!";
            }
            else
            {
                return resultString + "Learn for: " + ExamendToLearn.Name;
            }
        }

    }
}