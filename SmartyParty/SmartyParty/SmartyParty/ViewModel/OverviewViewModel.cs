﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using SmartyParty.Model;
using SmartyParty.ViewModel;

namespace SmartyParty.ViewModel
{
    class OverviewViewModel : BaseViewModel
    {
        public string Title { get; } = "Uebersicht";
        public ExamenCalender Data => ExamenCalender.GetInstance();
        public ObservableCollection<Day> DayOfLern => Data.DaysOfLerningtime;

        public OverviewViewModel()
        {
        }

        
    }
}
