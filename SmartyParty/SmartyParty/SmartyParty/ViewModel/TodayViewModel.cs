﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using SmartyParty.Model;
using Xamarin.Forms;

namespace SmartyParty.ViewModel
{
    class TodayViewModel : BaseViewModel
    {

        public string IsCheckedText { get {
            if (Data.Today.isChecked)
            {
                return "Doch nicht gemacht?";
            }return "Erledigt";
        } }
        public ExamenCalender Data => ExamenCalender.GetInstance();

        public TodayViewModel()
        {
        }

        public void OnChangeChecked(object sender, EventArgs args)
        {
            Data.Today.isChecked = !Data.Today.isChecked;
            ((Button) sender).Text = IsCheckedText;
        }
    }
}
