﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Scheduling
{
    class Program
    {
        static void Main(string[] args)
        {
            var cal = new ExamenCalender();

            cal.AddExamen(new Examen(new DateTime(2017, 03, 30), "A", 3));
            cal.AddExamen(new Examen(new DateTime(2017, 3, 29), "B", 3));
            cal.AddExamen(new Examen(new DateTime(2017, 3, 20), "X", 6));

            cal._recalculate();
        }
    }
}
