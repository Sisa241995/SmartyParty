﻿using System;

namespace Scheduling
{
    class Examen : BaseModel
    {
        public DateTime Date { get; private set; }
        public string Name { get; private set; }
        public int DaysToLearn { get; private set; }
        private int _daysLearned;

        public int DaysLearned
        {
            get
            {
                return _daysLearned;
            } 
            set
            {
                if (value >= 0) _daysLearned = value;
            }
        }

        public Examen(DateTime date, string name, int daysToLearn)
        {
            Date = date;
            Name = name;
            DaysToLearn = daysToLearn;
            DaysLearned = 0;
        }
    }
}
