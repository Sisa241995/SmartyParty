﻿using System;

namespace Scheduling
{
    class Day : BaseModel
    {
        public DateTime Date { get; private set; }
        public Examen ExamendToLearn { get; set; }
        private Boolean _isChecked;
        public bool isExaminationDay { get; set; }

        public Boolean isChecked
        {
            get
            {
                return _isChecked;
            } 
            set
            {
                decreaseOrIncreaseLearnedDays(value);
                _isChecked = value;
            }
        }

        private void decreaseOrIncreaseLearnedDays(bool value)
        {
            if (ExamendToLearn != null)
            {
                if (value)
                {
                    ExamendToLearn.DaysLearned = ExamendToLearn.DaysLearned + 1;
                }
                else
                {
                    ExamendToLearn.DaysLearned = ExamendToLearn.DaysLearned + 1;
                }
            }
        }

        public Day(DateTime date)
        {
            Date = date;
            isChecked = false;
            ExamendToLearn = null;
            isExaminationDay = false;
        }

        public override string ToString()
        {
            var resultString = "Date: " + this.Date + ": ";

            if (isExaminationDay)
            {
                return resultString + "EXAM TODAY: " + ExamendToLearn.Name;
            }

            if (this.ExamendToLearn == null)
            {
                return resultString + "Free!";
            }
            else
            {
                return resultString + "Learn for: " + ExamendToLearn.Name;
            }
        }
    }
}